import React from 'react';
import { Route, Link, Switch, Redirect } from 'react-router-dom';

import Header from "../common/layout/header";
import Footer from "../common/layout/footer";
import { PrivateRoute } from "../common/component/privateroute/PrivateRoute";

import About from "./about/About";
import Home from "./home/Home";
import Login from "./login/Login";
import PartList from './parts/PartList';
import ContractList from './contracts/ContractList';
import PartForm from './parts/PartForm';
import ContractForm from './contracts/ContractForm';

class App extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            logged: false
        }
    }

    render() {

        return (
            <div className="container-fluid">
                <Header />
                <div className="row marketing">
                    <Switch>
                        <Route path="/login" component={Login}/>
                        <PrivateRoute exact={true} path="/" component={Home} />
                        <PrivateRoute path="/contract/list" component={ContractList} />
                        <PrivateRoute path="/contract/form/:id?" component={ContractForm} />
                        <PrivateRoute path="/part/list" component={PartList} />
                        <PrivateRoute path="/part/form/:id?" component={PartForm} />
                        <PrivateRoute path="/about" component={About} />
                    </Switch>
                </div>
                <Footer/>
            </div>
        )
    }
}

export default App;
