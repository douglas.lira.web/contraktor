import React from 'react';
import { withRouter } from "react-router";
import { connect } from "react-redux";
import { loadParts, filterParts, deleteParts } from "../../redux/actions/partAction";
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faTrash} from "@fortawesome/free-solid-svg-icons";

library.add(faEdit, faTrash);

function mapDispatchToProps(dispatch) {
    return {
        loadParts: () => dispatch(loadParts()),
        filterParts: o => dispatch(filterParts(o)),
        deleteParts: obj => dispatch(deleteParts(obj))
    };
}

const mapStateToProps = state => {
    return { 
        items: state.parts
    };
};

class PartList extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            tpmSearch: ''
        }
    }

    componentWillMount() {
        this.props.loadParts();
    }

    handleChange(event) {
        this.setState({ [event.target.id]: event.target.value });
    }

    filterPart(o) {
        this.setState({ tpmSearch: o});
        this.props.filterParts(o);
    }

    add() {
        this.props.history.push("/part/form");
    }

    remove(id) {
        this.props.deleteParts(id);
    }

    edit(id) {
        this.props.history.push(`/part/form/${id}`);
    }
    
    render() {

        let { items } = this.props;

        return (
            <div className="container-fluid">
                <h4>Parts</h4>
                <div className="input-group mb-3">
                    <input type="text" className="form-control" value={this.state.tpmSearch} onChange={event => this.filterPart(event.target.value)} placeholder="Search by part" />
                    <div className="input-group-append">
                        <button className="btn btn-outline-primary" type="button" onClick={() => this.add()}>Register</button>
                    </div>
                </div>
                <table className="table">
                    <thead className="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">First name</th>
                            <th scope="col">Last name</th>
                            <th scope="col">Mail</th>
                            <th scope="col">CPF</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                    { items.parts.length && !items.isFetching ? (
                        items.parts.map((part) => 
                            <tr key={part.id}>
                                <th scope="row">{part.id}</th>
                                <td>{part.firstName}</td>
                                <td>{part.lastName}</td>
                                <td>{part.mail}</td>
                                <td>{part.cpf}</td>
                                <td>{part.phone}</td>
                                <td>
                                    <div className="btn-group" role="group">
                                        <button type="button" className="btn btn-primary" onClick={() => this.edit(part.id)}><FontAwesomeIcon icon={faEdit} /></button>
                                        <button type="button" className="btn btn-danger" onClick={() => this.remove(part.id)}><FontAwesomeIcon icon={faTrash} /></button>
                                    </div>
                                </td>
                            </tr>
                        )
                    ) : <tr><td colSpan="7">{ items.parts.length === 0 && !items.isFetching ? "No registered parts." : "Loading..." } </td></tr> }
                    </tbody>
                </table>
            </div>
        )
    }
}

const PList = connect(mapStateToProps, mapDispatchToProps)(PartList);

export default withRouter(PList);