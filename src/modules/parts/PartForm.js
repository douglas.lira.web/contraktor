import React from 'react';
import { withRouter } from "react-router";
import { connect } from "react-redux";
import { addParts, updateParts } from "../../redux/actions/partAction";

function mapDispatchToProps(dispatch) {
    return {
        addParts: part => dispatch(addParts(part)),
        updateParts: part => dispatch(updateParts(part))
    };
}

const mapStateToProps = state => {
    return { items: state.parts };
};

class Part extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            id: this.props.match.params.id || '',
            firstName: '',
            lastName: '',
            mail: '',
            cpf: '',
            phone: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentWillMount() {
        let partSelected = {};
        let partID = this.props.match.params.id || 0;
        if(partID) {
            partSelected = this.props.items.parts.filter(function(item){
                return item.id === parseInt(partID);
            })[0];
            this.setState(partSelected);
        }
    }

    cancel() {
        this.props.history.push("/part/list");
    }

    handleChange(event) {
        this.setState({ [event.target.id]: event.target.value });
    }

    handleSubmit(event) {
        event.preventDefault();
        let params = this;
        if(params.state.id) {
            this.props.updateParts(params.state);
        } else {
            params.state.id = new Date().getUTCMilliseconds();
            this.props.addParts(params.state);
        }
        params.setState({
            id: new Date().getUTCMilliseconds(),
            firstName: '',
            lastName: '',
            mail: '',
            cpf: '',
            phone: ''
        });
        params.props.history.push("/part/list");
    };

    render() {

        let { id } = this.state;

        return (
            <div className="container">
                <form onSubmit={this.handleSubmit}>
                    <div className="form-row">
                        <div className="form-group col-md-4">
                            <label>First name</label>
                            <input type="text" className="form-control" value={this.state.firstName} onChange={event => this.setState({ firstName: event.target.value })} placeholder="First name" />
                        </div>
                        <div className="form-group col-md-4">
                            <label>Last name</label>
                            <input type="text" className="form-control" value={this.state.lastName} onChange={event => this.setState({ lastName: event.target.value })} placeholder="Last name" />
                        </div>
                        <div className="form-group col-md-4">
                            <label>Phone</label>
                            <input type="text" className="form-control" value={this.state.phone} onChange={event => this.setState({ phone: event.target.value })} placeholder="Phone number" />
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <label>Mail</label>
                            <input type="text" className="form-control" value={this.state.mail} onChange={event => this.setState({ mail: event.target.value })} placeholder="Mail" />
                        </div>
                        <div className="form-group col-md-6">
                            <label>CPF</label>
                            <input type="text" className="form-control" value={this.state.cpf} onChange={event => this.setState({ cpf: event.target.value })} placeholder="CPF" />
                        </div>
                    </div>
                    <div className="btn-toolbar" role="toolbar">
                        <div className="btn-group mr-1" role="group">
                            <button type="submit" className="btn btn-primary">{ id ? 'Update' : 'Register'}</button>
                        </div>
                        <div className="btn-group" role="group">
                            <button type="button" className="btn btn-danger" onClick={() => this.cancel()}>Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

const PartForm = connect(mapStateToProps, mapDispatchToProps)(Part);

export default withRouter(PartForm);