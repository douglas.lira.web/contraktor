import config from 'config';

export const PartService = {
    filterParts,
    getParts,
    addParts,
    updateParts,
    deleteParts
};

function filterParts(o) {

    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };

    return fetch(`${config.apiUrl}/parts`, requestOptions).then(handleResponse).then(response => {
        let newList = [];
        if (o) {
            newList = response.filter((item) => {
                return item.firstName.includes(o) || item.lastName.includes(o) || item.cpf.includes(o) || item.mail.includes(o) || item.phone.includes(o) 
            })
        } else {
            newList = response;
        }
        return newList;
    });

}

function getParts(id) {

    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };

    return fetch(`${config.apiUrl}/parts`, requestOptions).then(handleResponse).then(response => {
        let newList = [];
        if (id) {
            newList = response.filter((item) => {
                return item.id === parseInt(id)
            })
        } else {
            newList = response;
        }
        return newList;
    });

}

function getParts(id) {

    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };

    return fetch(`${config.apiUrl}/parts`, requestOptions).then(handleResponse).then(response => {
        let newList = [];
        if (id) {
            newList = response.filter((item) => {
                return item.id === parseInt(id)
            })
        } else {
            newList = response;
        }
        return newList;
    });

}

function addParts(obj) {

    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(obj)
    };

    return fetch(`${config.apiUrl}/parts`, requestOptions).then(handleResponse).then(response => {
        if (response) {
            response.status = true;
        }
        return response;
    });

}

function updateParts(obj) {

    const requestOptions = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(obj)
    };

    return fetch(`${config.apiUrl}/parts`, requestOptions).then(handleResponse).then(response => {
        if (response) {
            response.status = true;
        }
        return response;
    });

}

function deleteParts(obj) {

    const requestOptions = {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(obj)
    };

    return fetch(`${config.apiUrl}/parts`, requestOptions).then(handleResponse).then(response => {
        return response;
    });

}

function handleResponse(obj) {
    return obj.result().then((response) => {
        const data = response && JSON.parse(response);
        if (!obj.ok) {
            const error = (data && data.message) || obj.statusText;
            return Promise.reject(error);
        }
        return data;
    });
}