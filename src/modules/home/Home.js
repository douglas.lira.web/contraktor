import React from 'react';

class Home extends React.Component {

    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <div className="container">
                <div className="jumbotron">
                    <h1 className="display-3">Contracktor 1.0</h1>
                    <p className="lead">By Douglas Lira</p>
                </div>
            </div>
        )
    }
}

export default Home;