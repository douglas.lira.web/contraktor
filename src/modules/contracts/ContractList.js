import React from 'react';
import { withRouter } from "react-router";
import { connect } from "react-redux";
import { loadContracts, filterContracts, deleteContracts } from "../../redux/actions/contractAction";
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faTrash, faEye} from "@fortawesome/free-solid-svg-icons";
import ReactModal from 'react-modal';
import { Document, Page } from 'react-pdf';

library.add(faEdit, faEye, faTrash);
ReactModal.setAppElement('#app')

function mapDispatchToProps(dispatch) {
    return {
        loadContracts: () => dispatch(loadContracts()),
        filterContracts: o => dispatch(filterContracts(o)),
        deleteContracts: obj => dispatch(deleteContracts(obj))
    };
}

const mapStateToProps = state => {
    return { 
        items: state.contracts
    };
};

class ContractList extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            tpmSearch: '',
            showModal: false,
            numPages: null,
            pageNumber: 1,
        }

        this.handleOpenModal = this.handleOpenModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.onDocumentLoadSuccess = this.onDocumentLoadSuccess.bind(this);
    }

    componentWillMount() {
        this.props.loadContracts();
    }

    onDocumentLoadSuccess({ numPages }) {
        this.setState({ numPages });
    }

    handleOpenModal () {
        this.setState({ showModal: true });
    }
    
    handleCloseModal () {
        this.setState({ showModal: false });
    }

    handleChange(event) {
        this.setState({ [event.target.id]: event.target.value });
    }

    filterContract(o) {
        this.setState({ tpmSearch: o});
        this.props.filterContracts(o);
    }

    add() {
        this.props.history.push("/contract/form");
    }

    remove(id) {
        this.props.deleteContracts(id);
    }

    edit(id) {
        this.props.history.push(`/contract/form/${id}`);
    }
    
    render() {

        let { items } = this.props;
        const { pageNumber, numPages } = this.state;

        return (
            <div className="container-fluid">
                <h4>Contratcs</h4>
                <div className="input-group mb-3">
                    <input type="text" className="form-control" value={this.state.tpmSearch} onChange={event => this.filterContract(event.target.value)} placeholder="Search by title" />
                    <div className="input-group-append">
                        <button className="btn btn-outline-primary" style={{zIndex:0}} type="button" onClick={() => this.add()}>Register</button>
                    </div>
                </div>
                <table className="table">
                    <thead className="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Date Start</th>
                            <th scope="col">Date Finish</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                    { items.contracts.length && !items.isFetching ? (
                        items.contracts.map((contract) => 
                            <tr key={contract.id}>
                                <th scope="row">{contract.id}</th>
                                <td>{contract.title}</td>
                                <td>{contract.dateStart}</td>
                                <td>{contract.dateEnd}</td>
                                <td>
                                    <div className="btn-group" role="group">
                                        <button type="button" className="btn btn-primary" onClick={() => this.edit(contract.id)}><FontAwesomeIcon icon={faEdit} /></button>
                                        <button type="button" className="btn btn-info" onClick={() => this.handleOpenModal(contract.file)}><FontAwesomeIcon icon={faEye} /></button>
                                        <button type="button" className="btn btn-danger" onClick={() => this.remove(contract.id)}><FontAwesomeIcon icon={faTrash} /></button>
                                    </div>
                                </td>
                            </tr>
                        )
                    ) : <tr><td colSpan="5">{ items.contracts.length === 0 && !items.isFetching ? "No contracts registered." : "Loading..." } </td></tr> }
                    </tbody>
                </table>

                <ReactModal isOpen={this.state.showModal} onRequestClose={this.handleCloseModal} shouldCloseOnOverlayClick={true}>
                    <Document file="../../assets/file/file.pdf" onLoadSuccess={this.onDocumentLoadSuccess}>
                        <Page pageNumber={pageNumber} />
                    </Document>
                    <p>Page {pageNumber} of {numPages}</p>
                </ReactModal>

            </div>
        )
    }
}

const CList = connect(mapStateToProps, mapDispatchToProps)(ContractList);

export default withRouter(CList);