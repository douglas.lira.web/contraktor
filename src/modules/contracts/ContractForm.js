import React from 'react';
import { withRouter } from "react-router";
import { connect } from "react-redux";
import { addContracts, updateContracts, addContractPart } from "../../redux/actions/contractAction";
import { loadParts } from "../../redux/actions/partAction";
import Autocomplete from "../../common/component/autocomplete/Autocomplete";
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash} from "@fortawesome/free-solid-svg-icons";

library.add(faTrash);

function mapDispatchToProps(dispatch) {
    return {
        addContracts: contract => dispatch(addContracts(contract)),
        updateContracts: contract => dispatch(updateContracts(contract)),
        addContractPart: part => dispatch(addContractPart(part)),
        loadParts: () => dispatch(loadParts())
    };
}

const mapStateToProps = state => {
    return { items: state.contracts, parts: state.parts.parts };
};

class Contract extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            id: this.props.match.params.id || '',
            title: '',
            dateStart: '',
            dateEnd: '',
            file: '',
            partsTmp: []
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentWillMount() {

        this.props.loadParts();
        
        let contractSelected = {};
        let contractID = this.props.match.params.id || 0;
        if(contractID) {
            contractSelected = this.props.items.contracts.filter(function(item){
                return item.id === parseInt(contractID);
            })[0];
            contractSelected.partsTmp = contractSelected.parts;
            this.setState(contractSelected);
        }
    }

    cancel() {
        this.props.history.push("/contract/list");
    }

    clickItem(obj) {
        let newObj = {
            id: parseInt(obj.id),
            firstName: obj.firstname,
            lastName: obj.lastname,
            mail: obj.mail
        }
        let listOfParts = this.state;
        let exists = Object.keys(listOfParts.partsTmp).some(function(k) {
            return listOfParts.partsTmp[k].id === newObj.id;
        });
        if(!exists) { 
            this.setState({
                partsTmp: this.state.partsTmp.concat(newObj)
            });
        }
    }

    removePart(obj) {
        this.setState({
            partsTmp: this.state.partsTmp.filter((item)=>{ return item.id !== obj})
        })
    }

    handleChange(event) {
        this.setState({ [event.target.id]: event.target.value });
    }

    handleSubmit(event) {
        event.preventDefault();
        let params = this;
        if(params.state.id) {
            this.props.updateContracts(params.state);
        } else {
            params.state.id = new Date().getUTCMilliseconds();
            this.props.addContracts(params.state);
        }
        params.setState({
            id: new Date().getUTCMilliseconds(),
            title: '',
            dateStart: '',
            dateEnd: '',
            file: '',
            partsTmp: []
        });
        params.props.history.push("/contract/list");
    };

    render() {

        let { id, partsTmp } = this.state;
        let { parts } = this.props;

        return (
            <div className="container">
                <form onSubmit={this.handleSubmit}>
                    <div className="form-row">
                        <div className="form-group col-md-12">
                            <label>Title</label>
                            <input type="text" className="form-control" value={this.state.title} onChange={event => this.setState({ title: event.target.value })} placeholder="First name" />
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="form-group col-md-4">
                            <label>Date Start</label>
                            <input type="date" className="form-control" value={this.state.dateStart} onChange={event => this.setState({ dateStart: event.target.value })} placeholder="Date Start" />
                        </div>
                        <div className="form-group col-md-4">
                            <label>Date End</label>
                            <input type="date" className="form-control" value={this.state.dateEnd} onChange={event => this.setState({ dateEnd: event.target.value })} placeholder="Date end" />
                        </div>
                        <div className="form-group col-md-4">
                            <label>File</label>
                            <input type="file" className="form-control" />
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="form-group col-md-12">
                            <label>Find participants</label>
                            <Autocomplete inputPlaceholder={'Search for participants to relate to this contract'} suggestions={parts} onClick={this.clickItem.bind(this)} fieldsFilter={['firstName', 'lastName']} fieldsShow={[['firstName', 'lastName']]}/>
                        </div>
                        <div className="form-group col-md-12">
                            <label>List of participants</label>
                            <div className="list-group">

                                {
                                    partsTmp.length ? (
                                        partsTmp.map((item) => 
                                            <div key={item.id} className="list-group-item list-group-item-action">
                                                <div className="row">
                                                    <div className="col-11">
                                                        <div>{item.firstName} {item.lastName}</div>
                                                        <small>{item.mail}</small>
                                                    </div>
                                                    <div className="col-1">
                                                        <div className="btn-group" role="group">
                                                            <button type="button" className="btn btn-danger" onClick={() => this.removePart(item.id)}><FontAwesomeIcon icon={faTrash} /></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    ) : <div className="list-group-item list-group-item-action"><div>No participants was added</div></div>
                                }

                            </div>
                        </div>

                    </div>
                    <div className="btn-toolbar" role="toolbar">
                        <div className="btn-group mr-1" role="group">
                            <button type="submit" className="btn btn-primary">{ id ? 'Update' : 'Register'}</button>
                        </div>
                        <div className="btn-group" role="group">
                            <button type="button" className="btn btn-danger" onClick={() => this.cancel()}>Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

const ContractForm = connect(mapStateToProps, mapDispatchToProps)(Contract);

export default withRouter(ContractForm);