import React from 'react';

class Footer extends React.Component {

    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <footer className="footer">
                <p>© Douglas Lira - 2019</p>
            </footer>
        )
    }
}

export default Footer;