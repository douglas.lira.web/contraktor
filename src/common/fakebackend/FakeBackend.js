export function FakeBackend() {
    
    let realFetch = window.fetch;
    let users = [
        { 
            id: 1,
            username: 'admin',
            password: 'admin',
            firstName: 'Douglas',
            lastName: 'Lira'
        }
    ];
    
    let contractList = [
        {
            id: 1,
            title: 'Contrato 01',
            dateStart: '2019-09-09',
            dateEnd: '2019-09-09',
            file: 'lista.pdf',
            parts: []
        }
    ];
    
    let partList = [
        {
            id: 1,
            firstName: 'Douglas',
            lastName: 'Lira',
            mail: 'douglas.lira.web@gmail.com',
            cpf: '066.189.376-63',
            phone: '+351 910 137 717'
        }
    ];
    
    window.fetch = function (url, opts) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {

                // CONTRACT ------------------------------------------------------------
                if (url.endsWith('/contracts') && opts.method === 'GET') {
                    resolve({ ok: true, result: () => Promise.resolve(JSON.stringify(contractList)) });
                    return;
                }

                if (url.endsWith('/contracts') && opts.method === 'PUT') {
                    let params = JSON.parse(opts.body);
                    if (params.title || params.id) {
                        let indexOfContract = contractList.findIndex((obj)=>{
                            return obj.id === params.id;
                        });
                        let responseJson = {
                            id: parseInt(params.id),
                            title: params.title,
                            dateStart: params.dateStart,
                            dateEnd: params.dateEnd,
                            file: params.file,
                            parts: params.parts
                        };
                        contractList[indexOfContract] = responseJson;
                        resolve({ ok: true, result: () => Promise.resolve(JSON.stringify(responseJson)) });
                    } else {
                        reject('Contract is incorrect');
                    }
                    return;
                }

                if (url.endsWith('/contracts') && opts.method === 'POST') {
                    let params = JSON.parse(opts.body);
                    if (params.title) {
                        let responseJson = {
                            id: contractList.length + 1,
                            title: params.title,
                            dateStart: params.dateStart,
                            dateEnd: params.dateEnd,
                            file: params.file,
                            parts: params.parts
                        };
                        contractList.push(responseJson);
                        resolve({ ok: true, result: () => Promise.resolve(JSON.stringify(responseJson)) });
                    } else {
                        reject('Contract is incorrect');
                    }
                    return;
                }

                if (url.endsWith('/contracts') && opts.method === 'DELETE') {
                    let params = JSON.parse(opts.body);
                    let indexOfPart = contractList.findIndex((obj)=>{
                        return obj.id === params.id;
                    });
                    contractList.splice(indexOfPart, 1);
                    resolve({ ok: true, result: () => Promise.resolve(JSON.stringify(contractList)) });
                    return;
                }

                // PARTS ------------------------------------------------------------
                if (url.endsWith('/parts') && opts.method === 'GET') {
                    resolve({ ok: true, result: () => Promise.resolve(JSON.stringify(partList)) });
                    return;
                }

                if (url.endsWith('/parts') && opts.method === 'PUT') {
                    let params = JSON.parse(opts.body);
                    if (params.firstName || params.lastName || params.id) {
                        let indexOfTask = partList.findIndex((obj)=>{
                            return obj.id === params.id;
                        });
                        let responseJson = {
                            id: parseInt(params.id),
                            firstName: params.firstName,
                            lastName: params.lastName,
                            mail: params.mail,
                            cpf: params.cpf,
                            phone: params.phone
                        };
                        partList[indexOfTask] = responseJson;
                        resolve({ ok: true, result: () => Promise.resolve(JSON.stringify(responseJson)) });
                    } else {
                        reject('Part is incorrect');
                    }
                    return;
                }

                if (url.endsWith('/parts') && opts.method === 'POST') {
                    let params = JSON.parse(opts.body);
                    if (params.firstName || params.lastName) {
                        let responseJson = {
                            id: partList.length + 1,
                            firstName: params.firstName,
                            lastName: params.lastName,
                            mail: params.mail,
                            cpf: params.cpf,
                            phone: params.phone
                        };
                        partList.push(responseJson);
                        resolve({ ok: true, result: () => Promise.resolve(JSON.stringify(responseJson)) });
                    } else {
                        reject('Part is incorrect');
                    }
                    return;
                }

                if (url.endsWith('/parts') && opts.method === 'DELETE') {
                    let params = JSON.parse(opts.body);
                    let indexOfPart = partList.findIndex((obj)=>{
                        return obj.id === params.id;
                    });
                    partList.splice(indexOfPart, 1);
                    resolve({ ok: true, result: () => Promise.resolve(JSON.stringify(partList)) });
                    return;
                }

                // AUTH -------------------------------------------------------------
                if (url.endsWith('/users/authenticate') && opts.method === 'POST') {
                    let params = JSON.parse(opts.body);
                    let filteredUsers = users.filter(user => {
                        return user.username === params.username && user.password === params.password;
                    });

                    if (filteredUsers.length) {
                        let user = filteredUsers[0];
                        let responseJson = {
                            id: user.id,
                            username: user.username,
                            firstName: user.firstName,
                            lastName: user.lastName
                        };
                        resolve({ ok: true, result: () => Promise.resolve(JSON.stringify(responseJson)) });
                    } else {
                        reject('Username or password is incorrect');
                    }

                    return;
                }

                realFetch(url, opts).then(response => resolve(response));
            }, 500);
        });
    }
}
