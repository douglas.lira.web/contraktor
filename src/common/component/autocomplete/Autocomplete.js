import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";

class Autocomplete extends Component {

    static propTypes() {
        this.suggestions = PropTypes.instanceOf(Array);
        this.fieldsFilter = PropTypes.instanceOf(Array);
        this.fieldsShow = PropTypes.instanceOf(Array);
    };

    static defaultProps() {
        this.suggestions = [];
        this.fieldsFilter = [];
        this.fieldsShow = [];
    };

    constructor(props, context) {
        super(props, context);
        this.state = {
            activeSuggestion: 0,
            filteredSuggestions: [],
            showSuggestions: false,
            userInput: ""
        };
        this.onChange = this.onChange.bind(this)
        this.onClick = this.onClick.bind(this)
    }

    onChange(e) {
        const { suggestions, fieldsFilter } = this.props;
        const userInput = e.currentTarget.value.toLowerCase();
        let results = [];
        suggestions.forEach((x) => {
            fieldsFilter.forEach((k)=>{
                if (x[k].toLowerCase().includes(userInput)) {
                    results.push(x);
                }
            });
        });
        const filteredSuggestions = [...new Set(results)];
        this.setState({
            activeSuggestion: 0,
            filteredSuggestions,
            showSuggestions: true,
            userInput: e.currentTarget.value
        });
    };

    onClick(e) {
        if(this.props.onClick){
            this.props.onClick(e.currentTarget.dataset);
        }
        this.setState({
            activeSuggestion: 0,
            filteredSuggestions: [],
            showSuggestions: false,
            userInput: ''
        });
    };

    render() {

        const {
            onChange,
            onClick,
            state: {
                activeSuggestion,
                filteredSuggestions,
                showSuggestions,
                userInput
            }
        } = this;
        const { fieldsShow, inputPlaceholder } = this.props;
        let suggestionsListComponent;

        if (showSuggestions && userInput) {
            if (filteredSuggestions.length) {
                suggestionsListComponent = (
                    <ul className="suggestions">
                        {filteredSuggestions.map((suggestion, index) => {
                            let className;
        
                            if (index === activeSuggestion) {
                                className = "suggestion-active";
                            }
        
                            return (
                                // * I can improve this. But it's already 03:00 and i'm tired!!
                                <li data-mail={suggestion.mail} data-id={suggestion.id} data-firstname={suggestion.firstName} data-lastname={suggestion.lastName} className={className} key={suggestion.id} onClick={onClick}>
                                    
                                    {
                                        fieldsShow.map((i, k) => {
                                            let uniqueName = '';
                                            for(var x = 0; x < i.length; x++){
                                                uniqueName += suggestion[i[x]]+" ";
                                            }
                                            return uniqueName;
                                        })
                                    }
                                    
                                    
                                </li>
                            );
                        })}
                    </ul>
                );
            } else {
                suggestionsListComponent = (
                    <div className="no-suggestions">
                      <em>No suggestions, you're on your own!</em>
                    </div>
                );
            }
        }

        return (
            <Fragment>
                <input type="text" className="form-control" placeholder={inputPlaceholder} onChange={onChange} value={userInput} />
                {suggestionsListComponent}
            </Fragment>
        )
    }
}

export default Autocomplete;