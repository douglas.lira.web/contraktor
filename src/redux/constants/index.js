export const LOAD_PART = "LOAD_PART";
export const FILTER_PART = "FILTER_PART";
export const ADD_PART = "ADD_PART";
export const UPDATE_PART = "UPDATE_PART";
export const DELETE_PART = "DELETE_PART";

export const LOAD_CONTRACT = "LOAD_CONTRACT";
export const FILTER_CONTRACT = "FILTER_CONTRACT";
export const ADD_CONTRACT = "ADD_CONTRACT";
export const UPDATE_CONTRACT = "UPDATE_CONTRACT";
export const DELETE_CONTRACT = "DELETE_CONTRACT";