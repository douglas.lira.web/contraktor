import { combineReducers } from 'redux';

import partReducer from './partReducer';
import contractReducer from './contractReducer';

export default combineReducers({
  parts: partReducer,
  contracts: contractReducer,
});