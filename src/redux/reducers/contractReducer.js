import { LOAD_CONTRACT, FILTER_CONTRACT, ADD_CONTRACT, UPDATE_CONTRACT, DELETE_CONTRACT } from "../constants";

const initialState = {
    contracts: [],
    isFetching: true,
    isError: false
};

export default function partReducer(state = initialState, action) {
    let contracts = [];
    switch (action.type) {
        case LOAD_CONTRACT:
            contracts = action.response;
            return { ...state, contracts, isFetching: false, isError: false };
        case FILTER_CONTRACT:
            contracts = action.response;
            return { ...state, contracts };
        case ADD_CONTRACT:
            contracts = state.contracts.concat(action.response);
            return { ...state, contracts };
        case UPDATE_CONTRACT:
            contracts = state.contracts[state.contracts.findIndex(el => el.id === action.response.id)] = action.response;
            return { ...state, contracts };
        case DELETE_CONTRACT:
            contracts = action.response;
            return { ...state, contracts };
        default:
            return state;
    }
}