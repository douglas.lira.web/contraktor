import { LOAD_PART, FILTER_PART, ADD_PART, UPDATE_PART, DELETE_PART } from "../constants";

const initialState = {
    parts: [],
    isFetching: true,
    isError: false
};

export default function partReducer(state = initialState, action) {
    let parts = [];
    switch (action.type) {
        case LOAD_PART:
            parts = action.response;
            return { ...state, parts, isFetching: false, isError: false };
        case FILTER_PART:
            parts = action.response;
            return { ...state, parts };
        case ADD_PART:
            parts = state.parts.concat(action.response)
            return { ...state, parts };
        case UPDATE_PART:
            parts = state.parts[state.parts.findIndex(el => el.id === action.response.id)] = action.response;
            return { ...state, parts };
        case DELETE_PART:
            parts = action.response;
            return { ...state, parts };
        default:
            return state;
    }
}