import { LOAD_CONTRACT, FILTER_CONTRACT, ADD_CONTRACT, UPDATE_CONTRACT, DELETE_CONTRACT } from "../constants/index";
import { ContractService } from "../../modules/contracts/service/ContractService";

export const filterContracts = (o) => {
  return (dispatch) => {
    return ContractService.filterContracts(o).then(response => {
        dispatch({type: FILTER_CONTRACT, response})  
      }).catch(error => {
        throw(error)
      });
  };
}


export const loadContracts = () => {
  return (dispatch) => {
    return ContractService.getContracts().then(response => {
        dispatch({type: LOAD_CONTRACT, response})  
      }).catch(error => {
        throw(error)
      });
  };
}

export const addContracts = (obj) => {
  obj.parts = obj.partsTmp;
  return (dispatch) => {
    return ContractService.addContracts(obj).then(response => {
        dispatch({type: ADD_CONTRACT, response})  
      }).catch(error => {
        throw(error)
      });
  };
}

export const updateContracts = (obj) => {
  obj.parts = obj.partsTmp;
  return (dispatch) => {
    return ContractService.updateContracts(obj).then(response => {
        dispatch({type: UPDATE_CONTRACT, response})  
      }).catch(error => {
        throw(error)
      });
  };
}

export const deleteContracts = (obj) => {
  return (dispatch) => {
    return ContractService.deleteContracts(obj).then(response => {
        dispatch({type: DELETE_CONTRACT, response})  
      }).catch(error => {
        throw(error)
      });
  };
}