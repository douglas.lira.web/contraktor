import { LOAD_PART, FILTER_PART, ADD_PART, UPDATE_PART, DELETE_PART } from "../constants/index";
import { PartService } from "../../modules/parts/service/PartService";

export const filterParts = (o) => {
  return (dispatch) => {
    return PartService.filterParts(o).then(response => {
        dispatch({type: FILTER_PART, response})  
      }).catch(error => {
        throw(error)
      });
  };
}


export const loadParts = () => {
  return (dispatch) => {
    return PartService.getParts().then(response => {
        dispatch({type: LOAD_PART, response})  
      }).catch(error => {
        throw(error)
      });
  };
}

export const addParts = (obj) => {
  return (dispatch) => {
    return PartService.addParts(obj).then(response => {
        dispatch({type: ADD_PART, response})  
      }).catch(error => {
        throw(error)
      });
  };
}

export const updateParts = (obj) => {
  return (dispatch) => {
    return PartService.updateParts(obj).then(response => {
        dispatch({type: UPDATE_PART, response})  
      }).catch(error => {
        throw(error)
      });
  };
}

export const deleteParts = (obj) => {
  return (dispatch) => {
    return PartService.deleteParts(obj).then(response => {
        dispatch({type: DELETE_PART, response})  
      }).catch(error => {
        throw(error)
      });
  };
}