const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const webpack = require("webpack");

module.exports = {
    mode: process.env.NODE_ENV === 'production' ? 'production' : 'development',
    output: {
        path: path.join(__dirname, '/dist'),
        filename: 'bundle.js'
    },
    module: {
        rules: [{
            test: /\.js$/,  
            exclude: /node_modules/,  
            use: {  
                loader: 'babel-loader',  
                options: {  
                    cacheDirectory: true  
                }  
            }  
        },{
            test: /\.html$/,
            use: [{
                loader: "html-loader",
                options: { minimize: true }
            }]
        },{
            test: /\.scss$/,
            use: [{ loader: MiniCssExtractPlugin.loader }, "css-loader", "sass-loader"]
        },{
            test: /\.(png|jpg)$/,
            use: {
              loader: "file-loader",
              options: {
                name: "[name].[ext]",
                outputPath: "images/"
              }
            }
        }, {
            test: /\.(pdf)$/,
            use: {
              loader: "file-loader",
              options: {
                name: "[name].[ext]",
                outputPath: "file/"
              }
            }
        }]
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Contraktor :: Douglas Lira',
            template: __dirname + '/template/index.html',
            meta: {
                viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no'
            },
            hash: true,
            inject: true
        }),
        new MiniCssExtractPlugin({
          filename: "css/[name].css"
        }),
        new webpack.WatchIgnorePlugin([
            path.join(__dirname, "node_modules")
        ])
    ],
    devtool: 'source-map',
    devServer: {
        //stats: "errors-only",
        historyApiFallback: true,
        inline: true,
        contentBase: 'src',
        open: true,
        overlay: true,
        watchOptions: {
            aggregateTimeout: 300,
            poll: 1000
        },
    },
    externals: {
        config: JSON.stringify({
            apiUrl: 'http://localhost:8080'
        })
    }
};